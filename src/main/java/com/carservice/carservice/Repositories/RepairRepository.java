package com.carservice.carservice.Repositories;

import com.carservice.carservice.Domain.Repair;
import com.carservice.carservice.Domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RepairRepository extends JpaRepository<Repair, Long> {

    List<Repair> findAll();


    @Query("SELECT userelid FROM Repair WHERE serviceid = :serviceid")
    User findOneUserId(@Param("serviceid")Long serviceid);


    List<Repair> findByUserelid_SsnStartingWith(String ssn);


   // List<Repair> findAllByDatetimeBetween(LocalDateTime servicedate);



}
package com.carservice.carservice.Exceptions;

public class AlreadySameException extends Exception {
	
	private static final long serialVersionUID= -47854848948798l;


    public AlreadySameException(String msg, Throwable t) {
        super(msg, t);
    }

    public AlreadySameException(String msg) {
        super(msg);
    }
}

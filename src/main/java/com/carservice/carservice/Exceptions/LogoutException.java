package com.carservice.carservice.Exceptions;

import org.springframework.security.core.AuthenticationException;

public class LogoutException extends AuthenticationException {
	
	private static final long serialVersionUID= -47885484848798l;


    public LogoutException(String msg, Throwable t) {
        super(msg, t);
    }

    public LogoutException(String msg) {
        super(msg);
    }

}

